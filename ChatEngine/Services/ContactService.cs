﻿using System;
using System.Collections.Generic;
using ChatEngine.Model;
using System.Threading.Tasks;

namespace ChatEngine.Services
{
    /// <summary>
    /// Provides information about contacts of ChatApp user.
    /// </summary>
    public class ContactService
    {
        private List<Contact> _contacts;

        public event EventHandler<ContactEventArgs> ContactUpdated;

        public ContactService()
        {
            _contacts = new List<Contact>();
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact A"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact B"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact C"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact D"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact E"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact F"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact G"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact H"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact I"));
            _contacts.Add(new Contact(_contacts.Count + 1, "Contact J"));
            StartContactChanges();
        }

        private async void StartContactChanges()
        {
            var iterations = 0;
            while (true)
            {
                await Task.Delay(17000);
                var random = new Random();
                var contactIndex = random.Next(_contacts.Count - 1);
                var oldContact = _contacts[contactIndex];
                var updatedContact = new Contact(oldContact.ContactId, oldContact.Name + " Update " + iterations);
                _contacts[contactIndex] = updatedContact;

                ContactUpdated?.Invoke(this, new ContactEventArgs(updatedContact));

                ++iterations;
            }
        }

        /// <summary>
        /// Loads all contacts.
        /// </summary>
        /// <returns>A list of contacts representing users the application</returns>
        public List<Contact> LoadAllContacts()
        {
            return _contacts;
        }
    }

    public class ContactEventArgs : EventArgs
    {
        public readonly Contact Contact;

        public ContactEventArgs(Contact contact)
        {
            Contact = contact;
        }
    }
}
