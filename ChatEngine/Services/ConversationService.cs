﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChatEngine.Model;

namespace ChatEngine.Services
{
    /// <summary>
    /// provides information about conversations and notifies about changes - conversation changes and arrivals of new messages.
    /// </summary>
    public class ConversationService
    {
        private readonly List<Conversation> _conversations;
        private readonly Dictionary<int, List<Message>> _messages;
        private int _conversationUpdateCount;

        /// <summary>
        /// Event that triggers when an existing conversation data is changed.
        /// </summary>
        public event EventHandler<ConversationEventArgs> ConversationUpdated;

        /// <summary>
        /// Event that triggers when a new conversation is created.
        /// </summary>
        public event EventHandler<ConversationEventArgs> ConversationAdded;

        /// <summary>
        /// Event that triggers when a new message is created for a conversation.
        /// </summary>
        public event EventHandler<MessageEventArgs> NewMessage;

        public ConversationService()
        {
            _messages = new Dictionary<int, List<Message>>();
            _conversations = new List<Conversation>();
            _conversations.Add(new Conversation(_conversations.Count, new List<int> {5, 8, 1}, "The first group chat", null));
            _messages.Add(_conversations[0].ConversationId, new List<Message>());
            _conversations.Add(new Conversation(_conversations.Count, new List<int> {1}, "One on one chat", null));
            _messages.Add(_conversations[1].ConversationId, new List<Message>());
            StartConversationChanges();
        }

        private async void StartConversationChanges()
        {
            while (true)
            {
                await TriggerConversationAdded();
                await TriggerConversationUpdated();
                await TriggerNewMessage();
            }
        }

        private async Task TriggerNewMessage()
        {
            await Task.Delay(13000);
            var random = new Random();
            var conversationWithMessage = _conversations[random.Next(_conversations.Count - 1)];
            var conversationParticipants = conversationWithMessage.ParticipantIds.Count;

            var sender = conversationWithMessage.ParticipantIds[random.Next(conversationParticipants - 1)];
            var conversationIndex = random.Next(_conversations.Count - 1);
            var message = new Message(conversationIndex, sender, "Message " + _messages.Count, DateTime.Now);
            var oldConversation = _conversations[conversationIndex];
            var updatedConversation = new Conversation(oldConversation.ConversationId, oldConversation.ParticipantIds,
                oldConversation.ConversationName + " Update " + ++_conversationUpdateCount, message);
            _conversations[conversationIndex] = updatedConversation;
            _messages[conversationIndex].Add(message);

            ConversationUpdated?.Invoke(this, new ConversationEventArgs(updatedConversation));

            NewMessage?.Invoke(this, new MessageEventArgs(message));
        }

        private async Task TriggerConversationUpdated()
        {
            await Task.Delay(11000);
            var random = new Random();
            var conversationIndex = random.Next(_conversations.Count - 1);
            var oldConversation = _conversations[conversationIndex];
            var updatedConversation = new Conversation(oldConversation.ConversationId, oldConversation.ParticipantIds, oldConversation.ConversationName + " Update " + ++_conversationUpdateCount, oldConversation.LatestMessage);
            _conversations[conversationIndex] = updatedConversation;

            ConversationUpdated?.Invoke(this, new ConversationEventArgs(updatedConversation));
        }

        private async Task TriggerConversationAdded()
        {
            await Task.Delay(7000);
            var random = new Random();
            var participantIds = new List<int>();
            var participantsCount = 1 + random.Next(5);
            for (var i = 0; i < participantsCount; i++)
            {
                participantIds.Add(1 + random.Next(10));
            }

            var newConversation = new Conversation(_conversations.Count, participantIds, "", null);
            _conversations.Add(newConversation);
            _messages.Add(newConversation.ConversationId, new List<Message>());

            ConversationAdded?.Invoke(this, new ConversationEventArgs(newConversation));
        }

        /// <summary>
        /// Method loads all conversations.
        /// </summary>
        /// <returns>A list of all conversations in the application</returns>
        public List<Conversation> LoadAllConversations()
        {
            return _conversations;
        }

        /// <summary>
        /// Method loads all conversation for a conversation with provided id.
        /// </summary>
        /// <param name="conversationId">Id of a conversation to load messages for</param>
        /// <returns>A list of messages for a conversation</returns>
        public List<Message> LoadAllMessagesForConversation(int conversationId)
        {
            return _messages[conversationId];
        }
    }

    public class ConversationEventArgs : EventArgs
    {
        public readonly Conversation Conversation;

        public ConversationEventArgs(Conversation conversation)
        {
            Conversation = conversation;
        }
    }

    public class MessageEventArgs : EventArgs
    {
        public readonly Message Message;

        public MessageEventArgs(Message message)
        {
            Message = message;
        }
    }
}
