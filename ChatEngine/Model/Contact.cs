﻿namespace ChatEngine.Model
{
    public class Contact
    {
        public readonly int ContactId;
        public readonly string Name;

        public Contact(int contactId, string name)
        {
            ContactId = contactId;
            Name = name;
        }
    }
}
