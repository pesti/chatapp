﻿using System.Collections.Generic;

namespace ChatEngine.Model
{
    public class Conversation
    {
        public readonly int ConversationId;
        public readonly string ConversationName;
        public readonly IReadOnlyList<int> ParticipantIds;
        public readonly Message LatestMessage;

        public Conversation(int conversationId, IReadOnlyList<int> participantIds, string conversationName, Message latestMessage)
        {
            ConversationId = conversationId;
            ParticipantIds = participantIds;
            ConversationName = conversationName;
            LatestMessage = latestMessage;
        }
    }
}
