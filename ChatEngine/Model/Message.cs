﻿using System;

namespace ChatEngine.Model
{
    public class Message
    {
        public readonly int ConversationId;
        public readonly int SenderId;
        public readonly string Text;
        public readonly DateTime Date;

        public Message(int conversationId, int senderId, string text, DateTime date)
        {
            ConversationId = conversationId;
            SenderId = senderId;
            Text = text;
            Date = date;
        }
    }
}
