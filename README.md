## ChatApp

Welcome to ChatApp. This is a test project intended for a software developer position interview.
How to use:

1. Fork the repository and make it private.
2. Open the ChatApp solution in Visual Studio.
3. Create a WPF application from the provided ChatApp template project.
4. Don't make any change in the ChatEngine project.
5. Push the changes into your fork and invite me to review it.

---

## App requirements

The application you need to build has the following requirements.

1. It is a master-detail application, with the list of conversations and list of messages for each conversation.
2. One single conversation in the list needs to show the following elements:
    * Name of a conversation if it has one, participant names separated by a ',' if conversation doesn't have its own name.
    * Text of latest message in the conversation (if there is one)
    * Date and time of the latest message (if there is one)
3. When a conversation in the list is selected, the list of messages for that conversation is shown in the details part of the UI.
4. List of messages is sorted by date ascending, with newest messages at the bottom.
5. Details part of the application should also show the name of selected conversation (same as it's shown in the list)
6. Application will update the UI with changes coming from the ChatEngine.

---

## ChatEngine API

To build the application you use the ChatEngine API. The API consists of two services and three domain classes.

* ContactService provides information about conversation participants.
    * List<Contact> LoadAllContacts(); returns list of contacts representing users the application.
    * event EventHandler<ContactEventArgs> ContactUpdated; Notifies when a contact data is changed.        
* ConversationService provides information about conversations and notifies about changes (conversation changes and arrivals of new messages)
    * event EventHandler<ConversationEventArgs> ConversationUpdated; Notifies when an existing conversation data is changed.
    * event EventHandler<ConversationEventArgs> ConversationAdded; Notifies when a new conversation is created.
    * event EventHandler<MessageEventArgs> NewMessage; Notifies when a new message is created for a conversation.